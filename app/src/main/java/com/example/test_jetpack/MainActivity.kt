package com.example.test_jetpack

import android.Manifest
import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.ThreadPolicy
import android.util.Log
import android.widget.RemoteViews
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.Person
import androidx.core.content.pm.ShortcutInfoCompat
import androidx.core.content.pm.ShortcutManagerCompat
import androidx.core.graphics.drawable.IconCompat
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.test_jetpack.route.RoutePath
import com.example.test_jetpack.screen.HomeScreen
import com.example.test_jetpack.screen.ProfileScreen
import com.example.test_jetpack.ui.theme.Test_jetpackTheme
import java.net.URL
import java.util.Date
import kotlin.random.Random


class MainActivity : ComponentActivity() {
    companion object {
        const val CHANNEL_ID: String = "OKE"
        const val ZAP_SHORTCUT_ID: String = "OKE11111111"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val policy = ThreadPolicy.Builder().permitAll().build()

        StrictMode.setThreadPolicy(policy)
//        Create Channel notification
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name: CharSequence = getString(R.string.channel_name)
            val description = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance)
            channel.description = description
            val notificationManager = getSystemService(
                NotificationManager::class.java
            )
            notificationManager.createNotificationChannel(channel)
        }
        setContent {
            Test_jetpackTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    ApplicationHost(showNotification = {
                        createNotification()
                    })
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    @RequiresApi(Build.VERSION_CODES.M)
    fun createNotification() {
        Thread.sleep(3000)
        val imageUrl =
            "https://img.freepik.com/free-photo/painting-mountain-lake-with-mountain-background_188544-9126.jpg"
        val url = URL(imageUrl)
        val image = BitmapFactory.decodeStream(url.openConnection().getInputStream())

        val notificationLayout = RemoteViews(packageName, R.layout.notification_small)
        val notificationLayoutExpanded = RemoteViews(packageName, R.layout.notification_large)

        notificationLayoutExpanded.setImageViewBitmap(R.id.image_noti, image)

//      Create Person show in notification
        val person = Person
            .Builder()
//            User name display
            .setName("Manh Tai")
            .build()


        val shortcut =
            ShortcutInfoCompat.Builder(this, ZAP_SHORTCUT_ID)
                .setLongLived(true)
                .setPerson(person)
//                Short label display near App Name notification
                .setShortLabel("Do Manh Tai")
                .setIcon(IconCompat.createWithBitmap(image))
                .setIntent(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://www.turnip.com/")
                    )
                )
                .build()
        ShortcutManagerCompat.pushDynamicShortcut(this, shortcut)

//        Create pending intent click notification
        var penDingFlag = PendingIntent.FLAG_UPDATE_CURRENT;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            penDingFlag = PendingIntent.FLAG_IMMUTABLE
        }
        val mBuilder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(IconCompat.createWithBitmap(image))
//            .setShortcutId(ZAP_SHORTCUT_ID)
            .setLargeIcon(image)
//            Auto delete notification when clicked
            .setAutoCancel(true)
//            Pending intent open screen when click notification
            .setContentIntent(
                PendingIntent.getActivity(
                    this, 0,
                    Intent(this, MainActivity::class.java), penDingFlag
                )
            )
//            Set style notification is message
//            .setStyle(
//                NotificationCompat.MessagingStyle(person)
//                    .addMessage("Content laf gif do" + Random(9999).nextInt(), Date().time, person)
//                    .addMessage("Content laf gif do" + Random(9999).nextInt(), Date().time, person)
//            )

//            .setStyle(NotificationCompat.DecoratedCustomViewStyle())
//            .setCustomContentView(notificationLayout)
//            .setCustomBigContentView(notificationLayoutExpanded)
        Log.d("11111111111111111", "createNotification: ")
//      Request permisstion
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.POST_NOTIFICATIONS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                requestPermissions(arrayOf(Manifest.permission.POST_NOTIFICATIONS), 10)
            }
            return
        }
//        Push notification
        val notificationId = Random.nextInt()
        val notificationManager = NotificationManagerCompat.from(this)
        notificationManager.notify(notificationId, mBuilder.build())
    }

}

@Composable
fun ApplicationHost(
    showNotification: () -> Unit
) {
    val navController = rememberNavController()
    return NavHost(
        navController = navController,
        startDestination = RoutePath.HOME
    ) {
        composable(RoutePath.HOME) {
            HomeScreen(navController = navController, showNotification)
        }
        composable(RoutePath.PROFILE) {
            ProfileScreen()
        }
    }
}


@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    Test_jetpackTheme {
    }
}