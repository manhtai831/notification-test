package com.example.test_jetpack.screen

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun ProfileScreen(){
    return ViewContent2()
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ViewContent2() {
    val msg = listOf<Int>(1, 2, 3, 4, 5, 6, 6, 7)
    return  Scaffold(
        topBar = {
            TopAppBar(title = {
                Text(text = "Profile")
            })
        },
        floatingActionButton = {
            FloatingActionButton(onClick = { /*TODO*/ }) {
                Icon(
                    Icons.Rounded.Add,
                    contentDescription = "Fab add"
                )
            }
        }

    ) {
        LazyColumn(modifier = Modifier.padding(it)) {
            items(
                count = msg.size,
                itemContent = {
                    ItemMessage()
                }
            )
        }
    }
}
