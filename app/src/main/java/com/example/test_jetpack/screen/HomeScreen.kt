package com.example.test_jetpack.screen

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.test_jetpack.route.RoutePath

@Composable
fun HomeScreen(
    navController: NavController,
    showNotification: () -> Unit
) {
    return ViewContent(navController,showNotification)
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ViewContent(
    navController: NavController,
    showNotification: () -> Unit
) {
    val msg = (1..200).toList()
//    val navController = rememberNavController()
    return Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = "Home")
                }
            )
        },
        floatingActionButton = {
            FloatingActionButton(onClick = {
                showNotification()
            }) {
                Icon(
                    Icons.Rounded.Add,
                    contentDescription = "Fab add"
                )
            }
        }

    ) {
        LazyColumn(modifier = Modifier.padding(it)) {
            items(
                count = msg.size,
                itemContent = {
                    ItemMessage()
                }
            )
        }
    }
}

@Composable
fun ItemMessage() {
    Box(modifier = Modifier.padding(24.dp)) {
        Text(text = "This is item message")
    }
}